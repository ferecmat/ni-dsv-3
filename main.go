package main

import (
	"context"
	"fmt"
	"log"
	"net"
	ring "ring/proto"
	"strconv"
	"sync"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type node struct {
	leftNode     ring.RingClient
	rightNode    ring.RingClient
	leftAddress  string
	rightAddress string
	id           string
	mu           *sync.Mutex
}

func NewNode(leftAddress, rightAddress, id string) *node {
	log.Printf("new node: %s < %s > %s\n", leftAddress, id, rightAddress)
	return &node{leftAddress: leftAddress, rightAddress: rightAddress, id: id, mu: &sync.Mutex{}}
}

func (n *node) Send(ctx context.Context, req *ring.Request) (*ring.Response, error) {
	log.Printf("received message: %s -> %s -> %s: %s\n", req.SenderId, n.id, req.ReceiverId, req.Content)

	if req.ReceiverId == n.id {
		log.Printf("message delivered to %s: %s\n", n.id, req.Content)
		return &ring.Response{Status: ring.Status_delivered}, nil
	}

	n.mu.Lock()
	defer n.mu.Unlock()

	switch req.Direction {
	case ring.Direction_left:
		n.leftNode.Send(context.Background(), req)
		return &ring.Response{Status: ring.Status_resent}, nil
	case ring.Direction_right:
		n.rightNode.Send(context.Background(), req)
		return &ring.Response{Status: ring.Status_resent}, nil
	}
	panic("unhandled case")
}

func (n *node) runServer(errs chan<- error) {
	listener, err := net.Listen("tcp", ":"+n.id)
	if err != nil {
		errs <- fmt.Errorf("unable to start server: %w", err)
		return
	}

	log.Printf("starting server on port: %s", n.id)

	s := grpc.NewServer()
	ring.RegisterRingServer(s, n)

	errs <- nil
	log.Fatalln(s.Serve(listener))
}

func (n *node) connectClients() error {
	leftCon, err := grpc.Dial(":"+n.leftAddress, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return fmt.Errorf("unable to create new connection: %w", err)
	}

	rightCon, err := grpc.Dial(":"+n.rightAddress, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return fmt.Errorf("unable to create new connection: %w", err)
	}

	n.leftNode = ring.NewRingClient(leftCon)
	n.rightNode = ring.NewRingClient(rightCon)
	return nil
}

func (n *node) sendMessage(req *ring.Request) {
	n.mu.Lock()
	defer n.mu.Unlock()
	switch req.Direction {
	case ring.Direction_left:
		n.leftNode.Send(context.Background(), req)
	case ring.Direction_right:
		n.rightNode.Send(context.Background(), req)
	}
}

func mod(a, b int) int {
	return (a%b + b) % b
}

func run() error {
	nodeCount := 5
	basePort := 5000
	nodes := make([]*node, nodeCount)

	for i := 0; i < nodeCount; i++ {
		nodes[i] = NewNode(
			strconv.Itoa(basePort+mod(i-1, nodeCount)),
			strconv.Itoa(basePort+mod(i+1, nodeCount)),
			strconv.Itoa(basePort+i))
	}

	errs := make(chan error)
	for _, node := range nodes {
		go node.runServer(errs)
	}

	log.Println("waiting for server start")
	for i := 0; i < nodeCount; i++ {
		if err := <-errs; err != nil {
			log.Println(err)
		}
	}

	log.Println("creating clients")

	for _, node := range nodes {
		if err := node.connectClients(); err != nil {
			return fmt.Errorf("unable to connect clients: %w", err)
		}
	}

	nodes[0].sendMessage(&ring.Request{
		ReceiverId: nodes[nodeCount-1].id,
		SenderId:   nodes[0].id,
		Content:    "hello from left",
		Direction:  ring.Direction_left})

	nodes[0].sendMessage(&ring.Request{
		ReceiverId: nodes[nodeCount-1].id,
		SenderId:   nodes[0].id,
		Content:    "hello from right",
		Direction:  ring.Direction_right})

	log.Println("exiting")
	return nil
}

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}
