# gRPC Ring

Implementation of two way ring using gRPC

## Building

If proto files are not generated, run the following commands

```console
make generate
```

## Running

```console
go run main.go
```

### Example output

```txt
2022/11/06 01:14:32 new node: 5004 < 5000 > 5001
2022/11/06 01:14:32 new node: 5000 < 5001 > 5002
2022/11/06 01:14:32 new node: 5001 < 5002 > 5003
2022/11/06 01:14:32 new node: 5002 < 5003 > 5004
2022/11/06 01:14:32 new node: 5003 < 5004 > 5000
2022/11/06 01:14:32 waiting for server start
2022/11/06 01:14:32 starting server on port: 5004
2022/11/06 01:14:32 starting server on port: 5000
2022/11/06 01:14:32 starting server on port: 5001
2022/11/06 01:14:32 starting server on port: 5003
2022/11/06 01:14:32 starting server on port: 5002
2022/11/06 01:14:32 creating clients
2022/11/06 01:14:32 received message: 5000 -> 5004 -> 5004: hello from left
2022/11/06 01:14:32 message delivered to 5004: hello from left
2022/11/06 01:14:32 received message: 5000 -> 5001 -> 5004: hello from right
2022/11/06 01:14:32 received message: 5000 -> 5002 -> 5004: hello from right
2022/11/06 01:14:32 received message: 5000 -> 5003 -> 5004: hello from right
2022/11/06 01:14:32 received message: 5000 -> 5004 -> 5004: hello from right
2022/11/06 01:14:32 message delivered to 5004: hello from right
2022/11/06 01:14:32 exiting
```
