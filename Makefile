generate: ring.proto
	mkdir -p proto
	protoc \
		--go_out=proto \
		--go_opt=paths=source_relative \
		--go-grpc_out=proto \
		--go-grpc_opt=paths=source_relative,require_unimplemented_servers=false \
		ring.proto


clean:
	ring
